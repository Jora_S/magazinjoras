package test4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 29.11.17
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */

public class ConsoleLoggingEventListener1 implements WebDriverEventListener {
    @Override
    public void beforeNavigateTo(String s, WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateTo(String s, WebDriver driver) {
        System.out.println("Open url:" + s); //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeNavigateBack(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateBack(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeNavigateForward(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateForward(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeNavigateRefresh(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterNavigateRefresh(WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeFindBy(By by, WebElement webElement, WebDriver driver) {
        System.out.println("Search for element:" + by.toString());
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterFindBy(By by, WebElement webElement, WebDriver driver) {
        System.out.println("Element found successfully");
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeClickOn(WebElement webElement, WebDriver driver) {
        System.out.println("Click on element:" + webElement.getTagName());
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver driver) {
        System.out.println("Element successfully clicked ");
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver driver, CharSequence[] charSequences) {
        System.out.println("Fill input" + webElement.getAttribute("id") + "with vally" + charSequences.toString());
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver driver, CharSequence[] charSequences) {
        System.out.println("Value successfully changed !!!");
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void beforeScript(String s, WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void afterScript(String s, WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
