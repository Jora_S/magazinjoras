package test4;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 30.11.17
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
public class AnnotationsExample {
    @BeforeTest
    public void beforeTest() { System.out.println("BeforeTest");}
    @AfterTest
    public void afterTest() { System.out.println("AfterTest");}


    @BeforeMethod
    public void beforeMethod() {
        System.out.println("BeforeMethod");}
    @AfterMethod
    public void afterMethod() {
        System.out.println("AfterMethod");}
    @Test
    public void test1() { System.out.println("Test1 execution");}
    @Test
    public void test2() { System.out.println("Test2 execution");}

    private void log(String message){
        Reporter.log(message + "<br>");
    }
}