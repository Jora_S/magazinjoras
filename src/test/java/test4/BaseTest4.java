package test4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 29.11.17
 * Time: 13:54
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseTest4 {
    public static void main(String[] args) {

        WebDriver driver = getConfiguredDriver() ;
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement field = driver.findElement(By.id("email"));
        field.sendKeys("webinar.test@gmail.com");
        WebElement field1 = driver.findElement(By.id("passwd"));
        field1.sendKeys(" Xcg7299bnSmMuRLp9ITw");
        WebElement button = driver.findElement(By.name("submitLogin"));
        button.click();
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        WebElement katalog = driver.findElement (By.cssSelector("#subtab-AdminCatalog > a > span"));
        Actions actions = new Actions(driver);
        actions.moveToElement(katalog).build().perform();
        WebElement tovari = driver.findElement(By.linkText("товары"));
        tovari.click();
        WebElement newtovar = driver.findElement(By.cssSelector("#page-header-desc-configuration-add > span")) ;
        newtovar.click();
        WebElement nazva = driver.findElement(By.id("form_step1_name_1"));
        nazva.sendKeys("Турецкие трусы");
        WebElement quantity = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        quantity.sendKeys("100");
        WebElement price = driver.findElement(By.id("form_step1_price_ttc_shortcut"));
        price.sendKeys("100");
        WebElement prosmotr = driver.findElement(By.cssSelector("#product_form_preview_btn"));
        prosmotr.click();
        System.out.println("Happy!!!!!");






    }

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        return  new ChromeDriver(options);
    }
    public static EventFiringWebDriver getConfiguredDriver() {
        EventFiringWebDriver driver = new EventFiringWebDriver(initChromeDriver());
        driver.register(new ConsoleLoggingEventListener1());
        return driver;



    }
}
