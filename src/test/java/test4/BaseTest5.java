package test4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 30.11.17
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public class BaseTest5 {
    public static void main(String[] args) {

        WebDriver driver = getConfiguredDriver() ;
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement field = driver.findElement(By.id("email"));
        field.sendKeys("webinar.test@gmail.com");
        WebElement field1 = driver.findElement(By.id("passwd"));
        field1.sendKeys(" Xcg7299bnSmMuRLp9ITw");
        WebElement button = driver.findElement(By.name("submitLogin"));
        button.click();
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        WebElement katalog = driver.findElement (By.cssSelector("#subtab-AdminCatalog > a > span"));
        Actions actions = new Actions(driver);
        actions.moveToElement(katalog).build().perform();
        WebElement tovari = driver.findElement(By.linkText("товары"));
        tovari.click();
        WebElement tovar = driver.findElement
                (By.xpath("//*[@id=\"product_catalog_list\"]/div[2]/div/table/tbody/tr[9]/td[3]/a"));
        System.out.println(tovar.getText());
        String tovarText = tovar.getText();
        assertEquals(tovarText, "Турецкие трусы");
        tovar.click();
         WebElement name3 = driver.findElement(By.name("form[step1][name][1]"));
        System.out.println(name3.getText());
        String name3Text = name3.getText();
        assertEquals(name3Text, "Турецкие трусы");




    }

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        return  new ChromeDriver(options);
    }
    public static EventFiringWebDriver getConfiguredDriver() {
        EventFiringWebDriver driver = new EventFiringWebDriver(initChromeDriver());
        driver.register(new ConsoleLoggingEventListener1());
        return driver;



    }
}
